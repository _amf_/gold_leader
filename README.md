# Overview #

This script is intended for looking up GDAX pricing when Ethereum shares are mined. Eventually I 
plan to expand this utility to produce single column accounting entries, which will assist with end 
of year taxes & also determine profitablilty.

# INSTALL #

This has been written to use python3.  At this time, setuptools is non-function.  
To install, create a virtualenv & pip install from requirements.txt

# Usage #

Once installed, modify the Ethereum wallet address to match your own in go_gold_leader.py. 
Finally, run go_gold_leader.py & look in the bin directory for a csv file.

# Notes #

This isn't ready for production.  For example, there's little to no logging available. Because 
of API requriements only returning 50 transactions per request, and no offset has been implemented in
this script, it's only good for printing the first 50 transactions from an Ethereum wallet. Additionally the 
historic prices API published by GDAX will sometimes not return data.   Exception handling needs to be 
added to prevent things from crashing and retry the request.   Should this be an issue, play with 'secs'
value in GDAX module. 