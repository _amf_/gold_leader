import gdax
import iso8601
import datetime
import logging
import time

class Exchange(object):
    def __init__(self):
        logging.basicConfig(level=logging.DEBUG)
        self.logger = logging.getLogger(__name__)
        self.gdax_eth = dict()
        self.gdax_btc = dict()

    def get_eth_historic_price_gdax(self, the_date=str(datetime.datetime.utcnow().isoformat())):
        '''
        Uses the GDAX API to take 120 one second samples from the time the block was mined.
        If there's an exception, one might have to increase the 'secs' value.

        Returns '[ time, low, high, open, close, volume ]' from gdax.public_client
        '''
        secs = 300
        client = gdax.PublicClient()
        mined_date = iso8601.parse_date(the_date)
        self.logger.debug("Date is: %s", mined_date)
        try:
            price = client.get_product_historic_rates("ETH-USD", start=mined_date,
                                                      end=mined_date+datetime.timedelta(seconds=secs), granularity=60)
            self.logger.debug("Price at %s is: %s", str(price[0]), str(price[2]))
            self.gdax_eth[the_date] = price[0]
        except Exception as e:
            raise e
        time.sleep(1)
        return self.gdax_eth