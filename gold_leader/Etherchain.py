import json
import urllib.request
import logging

class Chain(object):
    def __init__(self, wallet=None):
        """Initialize Chain object & optionally set the wallet address"""
        logging.basicConfig(level=logging.DEBUG)
        self.logger = logging.getLogger(__name__)
        self.address = wallet
        self.transactions = None
        self.fetch_status = None

    def set_wallet_address(self, wallet):
        """Set the objects wallet address"""
        self.address = wallet

    def old_fetch_wallet_transactions(self, offset=0):
        """Requests account information from etherchain.org API.  API limits results to 50 transactions. One may
           need to make multiple requests and update the offset."""
        try:
            if self.address == None:
                raise ValueError("wallet can not be 'None'")
            url = 'https://etherchain.org/api/account/{}/tx/{}'.format(self.address, offset)
            # TODO: change this 'print(url)' to a logger statement
            user_agent = (
                'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_5) AppleWebKit/603.2.4 (KHTML, like Gecko)' 
                'Version/10.1.1 Safari/603.2.4'
            )
            request = urllib.request.Request(url, data=None, headers={'User-Agent': user_agent})
            r_data = urllib.request.urlopen(request).read().decode('UTF-8')
            data = json.loads(r_data)           # needed as json.loads doesn't support multiple calls
            self.transactions = data['data']    # data['data'] is a list of dictionaries
            self.fetch_status = data['status']  # data['status'] is from API call and part of returned json

        except Exception as e:
            print(e)
            raise Exception (e)

    def fetch_wallet_transactions(self, offset=0):
        """Requests account information from etherchain.org API.  API limits results to 50 transactions. One may
           need to make multiple requests and update the offset."""
        try:
            if self.address == None:
                raise ValueError("wallet can not be 'None'")
            #url = 'https://etherchain.org/api/account/{}/tx/{}'.format(self.address, offset)
            #https://api.ethplorer.io/getAddressTransactions/0x08c3f1b454aefc02cc8ef158e27dccad7318a24f?apiKey=freekey&limit=50
            url = 'https://api.ethplorer.io/getAddressTransactions/{}?apiKey=freekey&limit={}'.format(self.address, "50")
            # TODO: change this 'print(url)' to a logger statement
            user_agent = (
                'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_5) AppleWebKit/603.2.4 (KHTML, like Gecko)'
                'Version/10.1.1 Safari/603.2.4'
            )
            request = urllib.request.Request(url, data=None, headers={'User-Agent': user_agent})
            r_data = urllib.request.urlopen(request).read().decode('UTF-8')
            data = json.loads(r_data)           # needed as json.loads doesn't support multiple calls
            self.transactions = data            # data['data'] is a list of dictionaries

        except Exception as e:
            print(e)
            raise Exception (e)


    def get_wallet_transactions(self):
        """Returns a dictionary of transactions"""
        return self.transactions