import logging
import datetime

class Wallet(object):
    """A Ethereum wallet object that contains an address and list of transactions."""
    def __init__(self, address=None):
        """Initialize the Wallet and set an address (or default the address to None)"""
        logging.basicConfig(level=logging.DEBUG)
        self.logger = logging.getLogger(__name__)
        self.address = address
        self.transactions = None

    def get_address(self):
        """Return the wallets address"""
        return self.address

    def set_address(self, address):
        """Override the address value with a new one"""
        self.address = address

    def set_transactions(self, transactions):
        """Writes transactions to the wallet"""
        self.transactions = transactions

    def get_transactions(self):
        """Gets stored transactions from the wallet"""
        return self.transactions

    def old_get_recepient_transactions(self, address):
        """ returns list of tuples containg time & ETH amount where recipient matches wallet address """
        # we multiply the block amount by 1.0e-18 to get decimal place correct. ETH has up to 18 digits & decimals
        # are not returned by this or etherscan.io API's.  (seems to also impact apps like GETH)
        my_blocks = []
        for block in self.transactions:
            if block['recipient'].upper() == address.upper():
                my_blocks.append((block['time'], block['amount'] * 1.0e-18))
        return my_blocks

    def get_recepient_transactions(self, address):
        """ returns list of tuples containg time & ETH amount where recipient matches wallet address """
        # we multiply the block amount by 1.0e-18 to get decimal place correct. ETH has up to 18 digits & decimals
        # are not returned by this or etherscan.io API's.  (seems to also impact apps like GETH)
        my_blocks = []
        for block in self.transactions:
            if block['to'].upper() == address.upper():
                my_blocks.append((datetime.datetime.fromtimestamp(block['timestamp']).isoformat(), block['value']))
        return my_blocks


    def _validate_address(self):
        """Validates that Ethereum address is valid (not implemented)"""
        # TODO: More info here: https://github.com/ethereum/EIPs/blob/master/EIPS/eip-55.md
        pass

