import unittest
from gold_leader import EthWallet

class TestEthWallet(unittest.TestCase):
    def setUp(self):
        self.wallet = EthWallet.Wallet()

    def test_is_EthWallet(self):
        self.assertIsInstance(self.wallet, EthWallet.Wallet, msg="Object is a EthWallet")


    def test_set_address(self):
        self.wallet.set_address("ABC123")
        self.assertTrue(self.wallet.address == "ABC123", msg="wallet address set to 'ABC123'")

    def test_get_address(self):
        self.wallet.set_address("ABC123")
        self.assertTrue(self.wallet.get_address() == "ABC123", msg="get_address() returns 'ABC123'")

if __name__ == '__main__':
    unittest.main()