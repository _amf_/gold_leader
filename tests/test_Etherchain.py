import unittest
from gold_leader import Etherchain

class TestEtherChain(unittest.TestCase):
    def setUp(self):
        self.chain = Etherchain.Chain()

    def test_is_EthChain(self):
        self.assertIsInstance(self.chain, Etherchain.Chain, msg="Object is  Etherchain.Chain")


    def test_set_wallet_address(self):
        self.chain.set_wallet_address("0x3B45f6a795814E27E3cad1558512794e1db2c975")
        self.assertTrue(self.chain.address == "0x3B45f6a795814E27E3cad1558512794e1db2c975",
                        msg="wallet address set to '0x3B45f6a795814E27E3cad1558512794e1db2c975'")

    def test_fetch_transactions(self):
        # TODO: This will require some mocking due to urllib requests.
        pass

    def test_get_transactions(self):
        # TODO: Make sure we get a dictionary of transactions back from the object
        pass

if __name__ == '__main__':
    unittest.main()