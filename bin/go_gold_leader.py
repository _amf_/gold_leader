#!/usr/bin/env python3
import sys
sys.path.append("..")


from gold_leader import EthWallet, Etherchain, ExchangeAPI
import csv

my_exchange = ExchangeAPI.Exchange()
wallet = EthWallet.Wallet()
wallet.set_address('0x08c3f1b454aefc02cc8ef158e27dccad7318a24f')

chain = Etherchain.Chain(wallet.get_address())
chain.fetch_wallet_transactions()

wallet.set_transactions(chain.get_wallet_transactions())
# wallet.transactions is now list of dictionary transactions
received_transactions = wallet.get_recepient_transactions(wallet.get_address())

with open("mining_profit.csv", 'w', newline='') as csvfile:
    field_names = ["time", "low", "high", "open", "close", "amount mined", "US dollar value (high)"]
    csv_writer = csv.DictWriter(csvfile, fieldnames=field_names)
    csv_writer.writeheader()

    for (transaction_time,transaction_amount) in received_transactions:
        # gets '{ time: [epoch, low, high, open, close, volume ]}' from ExcahangeAPI
        try:
            transaction_values = my_exchange.get_eth_historic_price_gdax(transaction_time)
        except KeyError:
            transaction_values = my_exchange.get_eth_historic_price_gdax(transaction_time)

        transaction_low = transaction_values[transaction_time][1]
        transaction_high = transaction_values[transaction_time][2]
        transaction_open = transaction_values[transaction_time][3]
        transaction_close = transaction_values[transaction_time][4]

        csv_writer.writerow({
                            "time": transaction_time, "low": transaction_low,
                            "high": transaction_high, "open": transaction_open,
                            "close": transaction_close, "amount mined": transaction_amount,
                            "US dollar value (high)": transaction_amount * transaction_high
                             })




