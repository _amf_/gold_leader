#!/usr/bin/env python3

import gdax
import iso8601
import datetime


# TODO: Read wallet address for input.  Can we somehow validate input?

# TODO: Use wallet address to get transactions from etherchain.org api.
#       Should this be from local geth instance?
#       What about adding support for other block chains, (ie: bitcoin),
#       it's all the same stuff after all ...
#       https://blockchain.info/api/blockchain_api

#  TODO: use etherchain.org to get timestamps in proper format and list of transactions
#        ie: https://etherchain.org/account/0x08c3f1b454aefc02cc8ef158e27dccad7318a24f#txreceived
#
#        can use etherchain API to get list of transactions
#        https://etherchain.org/documentation/api/#api-Accounts-GetAccountIdTxOffset
#        https://etherchain.org/api/account/:id/tx/:offset

#  TODO: Use GDAX to get price for that transaction.

public_client = gdax.PublicClient()
mined_date = iso8601.parse_date('2017-07-10 10:52:26')

price = public_client.get_product_historic_rates('ETH-USD', start=mined_date,
                                                  end=mined_date+datetime.timedelta(seconds=30), granularity=1)

time = datetime.datetime.fromtimestamp(price[0][0], datetime.timezone.utc)

print("Time: %s, low: %s, high: %s, open: %s, close %s, volume %s" %
      (time, price[0][1], price[0][2], price[0][3], price[0][4], price[0][5]))

